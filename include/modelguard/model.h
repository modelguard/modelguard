/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MG_MODEL_H_
#define MG_MODEL_H_

#include <Eigen/Dense>

namespace modelguard {

using RowMajorArray = Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
using RowVector = Eigen::Array<float, 1, Eigen::Dynamic, Eigen::RowMajor>;

class Model {
    public:
        // Accessors
        virtual Eigen::Ref<const Eigen::ArrayX2f> getStateSpace() const = 0;
        virtual Eigen::Ref<const Eigen::ArrayX2f> getInputSpace() const = 0;
        virtual Eigen::Ref<const Eigen::ArrayX2f> getOutputSpace() const = 0;
        
        virtual int getStateDim() const = 0;
        virtual int getInputDim() const = 0;
        virtual int getOutputDim() const = 0;

        virtual float getLipschitz() const = 0;
        virtual int getBatchSize() const = 0;

        // Execution
        virtual void initialize() = 0;
        virtual Eigen::Ref<RowMajorArray> invoke(const Eigen::Ref<const RowMajorArray> &state, const Eigen::Ref<const RowVector> &inputs) = 0;

        virtual ~Model() {}
        virtual Model* clone() const = 0;
};
} // endnamespace modelguard

#endif