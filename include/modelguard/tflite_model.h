/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MG_TFLITE_MODEL_
#define MG_TFLITE_MODEL_

#include <memory>

#include <tensorflow/lite/model.h>
#include <tensorflow/lite/interpreter.h>

#include <Eigen/Dense>

#include "modelguard/model_base.h"

namespace modelguard {
class TFLiteModel : public ModelBase {
    protected:
        std::shared_ptr<tflite::FlatBufferModel> tfmodel;

        std::unique_ptr<tflite::Interpreter> interpreter;
        std::unique_ptr<Eigen::Map<RowMajorArray>> inputAdaptor;
        std::unique_ptr<Eigen::Map<RowMajorArray>> outputAdaptor;

        float* inputBuffer = NULL;
        float* outputBuffer = NULL;

    public:
        TFLiteModel(std::shared_ptr<tflite::FlatBufferModel> tfmodel,
                    const Eigen::Ref<const Eigen::ArrayX2f> stateSpace,
                    const Eigen::Ref<const Eigen::ArrayX2f> inputSpace,
                    const Eigen::Ref<const Eigen::ArrayX2f> outputSpace,
                    const double lipschitz,
                    const int batchSize);
        TFLiteModel(const TFLiteModel &other);

        void initialize() override;
        Eigen::Ref<RowMajorArray> invoke(const Eigen::Ref<const RowMajorArray> &state, const Eigen::Ref<const RowVector> &inputs) override;

        TFLiteModel* clone() const override;
        ~TFLiteModel() override; 
};
} // endnamespace modelguard

#endif