/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MG_MODEL_BASE_H_
#define MG_MODEL_BASE_H_

#include <Eigen/Dense>

#include "modelguard/model.h"

namespace modelguard {
class ModelBase : public Model {
    protected:
        Eigen::ArrayX2f stateSpace;
        Eigen::ArrayX2f inputSpace;
        Eigen::ArrayX2f outputSpace;

        float lipschitz;
        int batchSize;

        bool initialized = false;

    public:
        ModelBase() {}
        ModelBase(const Eigen::Ref<const Eigen::ArrayX2f> stateSpace
                , const Eigen::Ref<const Eigen::ArrayX2f> inputSpace
                , const Eigen::Ref<const Eigen::ArrayX2f> outputSpace
                , const float lipschitz
                , const int batchSize) 
            : stateSpace(stateSpace)
            , inputSpace(inputSpace)
            , outputSpace(outputSpace)
            , lipschitz(lipschitz)
            , batchSize(batchSize)
        {}

        Eigen::Ref<const Eigen::ArrayX2f> getStateSpace() const override { return stateSpace; }
        Eigen::Ref<const Eigen::ArrayX2f> getInputSpace() const override { return inputSpace; }
        Eigen::Ref<const Eigen::ArrayX2f> getOutputSpace() const override { return outputSpace; }
        
        int getStateDim() const override { return stateSpace.rows(); }
        int getInputDim() const override { return inputSpace.rows(); }
        int getOutputDim() const override { return outputSpace.rows(); }

        float getLipschitz() const override { return lipschitz; }
        int getBatchSize() const override { return batchSize; }

        // Execution
        void initialize() override = 0;
        Eigen::Ref<RowMajorArray> invoke(const Eigen::Ref<const RowMajorArray> &state, const Eigen::Ref<const RowVector> &inputs) override = 0;

        ~ModelBase() override {}
        ModelBase* clone() const override = 0;
};
} // endnamespace modelguard

#endif