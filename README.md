# ModelGuard

## Introduction
ModelGuard is a tool for performing model validation on Lipschitz-continuous
models. Given a model and a set of inputs and outputs, ModelGuard determines
if the inputs/outputs could have been produced by the model, to a given epsilon
error. This is particularly useful in cases where a model has been used as part
of a design-time analysis and the results should be monitored at runtime.

ModelGuard is provided in two forms: first as a library that can be incorporated 
into other programs; and second as an executable that can readily evaluate models 
represented in the Tensorflow-Lite format.

## TFLite Executable
The ModelGuard approach can be applied to any Lipschitz-continuous model.
In the executable form, however, the tool only accepts neural network models 
stored in the TF-Lite (from Tensorflow) format.

### Model Architecture
ModelGuard accepts any neural network model, stored in the TF-Lite format,
that contains standard layers (e.g., layers that are built in to TF-Lite). The
neural network must accept parameters before the model inputs (e.g., for a
model with 4 parameters and 3 inputs, the neural network should contain the
following structure [p1, p2, p3, p4, i1, i2, i3]). The neural network must also
only produce the model outputs.

### Lipschitz Constant
A critical component of the ModelGuard approach is knowing an upper bound
on the Lipschitz constant of the model. There are multiple ways to compute the
Lipschitz constant. One method is to explicitly restrict the Lipschitz constant
during the training process, such as with https://github.com/henrygouk/
keras-lipschitz-networks. If a neural network has already been trained,
an analysis can be performed to calculate the Lipschitz constant, such as with
https://github.com/arobey1/LipSDP.

### Configuration File
The configuration file is a YAML file that provides all of the required informa-
tion for a ModelGuard evaluation, including the model meta-information, the
input/output trace to evaluate, the epsilon to evaluate for, as well as time and
iteration limits. The possible configuration values are presented in the table
below. Example configuration files are provided with this project.