/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <vector>
#include <math.h>

#include <Eigen/Dense>

#include <yaml-cpp/yaml.h>
#include <yaml-cpp/node/node.h>

#include <tensorflow/lite/model.h>
#include <tensorflow/lite/interpreter.h>

#include "modelguard/model.h"
#include "modelguard/tflite_model.h"
#include "modelguard/modelguard.h"
#include "modelguard/engine_config.h"
#include "modelguard/eval_options.h"

#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/c/common.h>

using namespace modelguard;
using namespace std;

unique_ptr<Model> parseModel(const YAML::Node &node) {
    auto stateSpaceNode = node["state-space"];
    int numDimensions = stateSpaceNode.size();

    if(stateSpaceNode[0].size() == 0) {
        numDimensions = 0;
    }

    Eigen::ArrayX2f stateSpace = Eigen::ArrayX2f::Zero(numDimensions,2);
    for( int i=0; i<numDimensions; i++ ) {
        stateSpace(i,0) = stateSpaceNode[i][0].as<float>();
        stateSpace(i,1) = stateSpaceNode[i][1].as<float>();
    }

    auto inputSpaceNode = node["input-space"];
    numDimensions = inputSpaceNode.size();

    if(inputSpaceNode[0].size() == 0) {
        numDimensions = 0;
    }

    Eigen::ArrayX2f inputSpace = Eigen::ArrayX2f::Zero(numDimensions, 2);
    for( int i=0; i<numDimensions; i++ ) {
        inputSpace(i,0) = inputSpaceNode[i][0].as<float>();
        inputSpace(i,1) = inputSpaceNode[i][1].as<float>();
    }

    auto outputSpaceNode = node["output-space"];
    numDimensions = outputSpaceNode.size();

    if(outputSpaceNode[0].size() == 0) {
        numDimensions = 0;
    }

    Eigen::ArrayX2f outputSpace = Eigen::ArrayX2f::Zero(numDimensions, 2);
    for( int i=0; i<numDimensions; i++ ) {
        outputSpace(i,0) = outputSpaceNode[i][0].as<float>();
        outputSpace(i,1) = outputSpaceNode[i][1].as<float>();
    }

    double lipschitz = node["lipschitz"].as<double>();
    int batch = 100;
    if(node["batch"]) {
        batch = node["batch"].as<int>();
    }

    unique_ptr<Model> model;
    std::string type = node["type"].as<std::string>();
    if( type == "nn" ) {
        std::string filename = node["filename"].as<std::string>();
        std::shared_ptr<tflite::FlatBufferModel> tflite = tflite::FlatBufferModel::BuildFromFile(filename.c_str());
        model = make_unique<TFLiteModel>(tflite, stateSpace, inputSpace, outputSpace, lipschitz, batch);
    } 

    return model;
}

RowMajorArray parseTrace(const YAML::Node &node) {
    int numDimensions = node.size();
    RowMajorArray trace(1, numDimensions);
    for (int i=0; i<numDimensions; i++) {
        trace(0,i) = node[i].as<float>();
    }

    return trace;
}

int main(int argc, char* argv[]) {
    YAML::Node config;

    if (argc < 2) {
        cout << "Config file not specified. Reading from standard input..." << endl;
        config = YAML::Load(cin);
    } else {
        config = YAML::LoadFile(argv[1]);
    }

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    if(config["seed"]) {
        seed = config["seed"].as<unsigned>();
    } 

    if(!config["model"]) {
        cerr << "Model specification 'model' missing from configuration file" << endl;
        return -2;
    }

    auto modelNode = config["model"];
    unique_ptr<Model> model = parseModel(modelNode);

    EngineConfig eConfig = EngineConfig::Default();

    if(!config["numBins"]) {
        cerr << "Number of discretization bins 'numBins' missing from configuration file" << endl;
        return -3;
    }
    eConfig.withBinningFactor(config["numBins"].as<long double>());

    if(!config["input-trace"]) {
        cerr << "Input trace to evaluate 'input-trace' missing from configuration file" << endl;
        return -4;
    }
    auto input = parseTrace(config["input-trace"]);

    if(!config["output-trace"]) {
        cerr << "Output trace to evaluate 'output-trace' missing from configuration file" << endl;
        return -5;
    }
    auto output = parseTrace(config["output-trace"]);

    EvalOptions options = EvalOptions::Default();

    if(!config["epsilon"]) {
        cerr << "Epsilon to evaluate with 'epsilon' missing from configuration file" << endl;
        return -6;
    }
    options.withEpsilon(config["epsilon"].as<float>());

    if(config["iteration-limit"]) {
        options.withIterationLimit(config["iteration-limit"].as<long int>());
    }

    if(config["time-limit"]) {
        chrono::duration<double> timeLimit(config["time-limit"].as<double>());
        options.withTimeLimit(chrono::duration_cast<chrono::nanoseconds>(timeLimit));
    }

    int progressFrequency = -1;
    if(config["progress-frequency"]) {
        options.withPeriodicProgress(config["progress-frequency"].as<int>());
    }

    if(config["progress-filename"]) {
        options.withProgressFile(config["progress-filename"].as<std::string>());
    }

    if(!config["deltas"]) {
        cerr << "Deltas to evaluate with 'deltas' missing from configuration file" << endl;
        return -7;
    }
    options.withDeltas(config["deltas"].as<vector<float>>());


    auto mg = ModelGuard::Create(model, eConfig);
    auto result = mg->evaluate(input, output, options);

    cout << "Consistent: " << result.consistent << endl;
    cout << "Confidences: " << endl;
    for(int i=0; i<options.deltas.size(); i++) {
        cout << "\t" << options.deltas[i] << " -> " << result.confidences[i] << endl;
    }
    cout << "Minimum: " << result.minimum << endl;
    cout << "Iterations: " << result.iterations << endl;
    cout << "Elapsed Time: " << result.elapsedTime << endl;
}