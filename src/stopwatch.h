/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <chrono>

namespace modelguard::impl {
class Stopwatch {
    public:
        using clock_t = std::chrono::high_resolution_clock;
        using time_t = std::chrono::time_point<clock_t>;

    protected:
        std::chrono::nanoseconds accTime = std::chrono::nanoseconds();
        std::chrono::nanoseconds accTimeLap = std::chrono::nanoseconds();

        int occurrences = 0;
        int occurrencesLap = 0;
        
        std::chrono::nanoseconds maxElapsed = std::chrono::nanoseconds();
        std::chrono::nanoseconds maxElapsedLap = std::chrono::nanoseconds();

        time_t localStart;
        bool running = false;

    public:
        void start();
        std::chrono::nanoseconds stop(bool ignore=false);
        std::chrono::nanoseconds lapReset();
        void reset();

        std::chrono::nanoseconds elapsed(bool lap=false) const;
        int totalOccurrences(bool lap=false) const;
        std::chrono::nanoseconds totalTime(bool lap=false) const;
        std::chrono::nanoseconds meanTime(bool lap=false) const;
        std::chrono::nanoseconds maxTime(bool lap=false) const;
        bool isRunning() const;
};
} // endnamespace modelguard::impl

#endif