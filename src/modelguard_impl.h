/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MG_MODELGUARD_IMPL_H_
#define MG_MODELGUARD_IMPL_H_

#include <memory>
#include <vector>
#include <functional>

#include <Eigen/Dense>

#include "modelguard/modelguard.h"
#include "modelguard/model.h"
#include "modelguard/engine_config.h"
#include "modelguard/eval_options.h"
#include "stopwatch.h"

namespace modelguard::impl {
class ModelGuardImpl : public ModelGuard {
    protected:
        std::unique_ptr<Model> model;
        const EngineConfig config;

        long double totalVolume;
        std::vector<std::function<float()>> randomizers;

        Stopwatch stopwatch;

    public:
        ModelGuardImpl(std::unique_ptr<Model> &model, const EngineConfig config);
        Result evaluate(const Eigen::Ref<const RowVector> &inputs, const Eigen::Ref<const RowVector> &outputs, const EvalOptions options) override;
        ~ModelGuardImpl() override;

    protected:
        long double calculateVolume(const Eigen::Ref<const Eigen::ArrayX2f> &space);
        void random(Eigen::Ref<RowMajorArray> result);
        long double calculateStateVolume(double distance, const Eigen::Ref<const RowMajorArray> state);
        long double calculateConfidence(const float delta, const long int K, const long double avgM);
};
}

#endif