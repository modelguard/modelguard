/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "modelguard_impl.h"

#include <memory>
#include <random>
#include <functional>
#include <map>
#include <iostream>
#include <fstream>

#include <Eigen/Dense>

#include "modelguard/model.h"
#include "modelguard/engine_config.h"
#include "modelguard/eval_options.h"

#include "stopwatch.h"

namespace modelguard::impl {

ModelGuardImpl::ModelGuardImpl(std::unique_ptr<Model> &model, const EngineConfig config) : config(config) {
    this->model.swap(model);
    this->model->initialize();

    this->totalVolume = calculateVolume(this->model->getStateSpace());

    for(int i = 0; i<this->model->getStateDim(); i++) {
        std::mt19937_64 generator;
        generator.seed(config.seed + i);
        this->randomizers.push_back(std::bind(std::uniform_real_distribution<float>(this->model->getStateSpace()(i,0), this->model->getStateSpace()(i,1)), generator));
    }
}

Result ModelGuardImpl::evaluate(const Eigen::Ref<const RowVector> &inputs, const Eigen::Ref<const RowVector> &outputs, const EvalOptions options) {
    stopwatch.start();

    std::ofstream fout;
    if(!options.progressFile.empty()) {
        fout.open(options.progressFile);

        fout << "ElapsedTime" << "," << "Iterations" << ",";
        for(int i=0; i<options.deltas.size(); i++) {
            fout << "Conf" << options.deltas[i] << ",";
        }
        fout << std::endl;
    }

    long double minimum = std::numeric_limits<long double>::infinity();
    long int iterations = 0;
    std::map<long int, long int> sampleBins;

    RowMajorArray state = RowMajorArray::Zero(model->getBatchSize(), model->getStateDim());
    while(minimum > options.epsilon && iterations < options.iterationLimit && stopwatch.elapsed() < options.timeLimit) {
        random(state);
        
        Eigen::Ref<RowMajorArray> modelOutput = model->invoke(state, inputs);

        for(int i=0; i<state.rows(); i++) {
            long double distance = (modelOutput.row(i) - outputs).abs().maxCoeff();
            
            if(distance < minimum) {
                minimum = distance;
            }

            iterations++;

            if(minimum <= options.epsilon) {
                break;
            } else {
                distance = (distance - options.epsilon) / (2 * model->getLipschitz());

                long int volume = floor(config.binningFactor * (calculateStateVolume(distance, state.row(i))/totalVolume));
                sampleBins[volume] += 1;
            }

            if(options.periodicProgress > 0 && iterations % options.periodicProgress == 0) {
                auto& out = (fout.is_open()) ? fout : std::cout;

                long double averageM = 0.0;
                if(sampleBins.size() == 0) {
                    averageM = 1;
                } else {
                    for( const auto& [key, val] : sampleBins ) {
                        averageM += pow(1.0 - (key/config.binningFactor), iterations) * val;
                    }
                    averageM = averageM / iterations;
                }
                //cout << "AVERAGE M: " << averageM << endl << endl;

                out << std::chrono::duration_cast<std::chrono::duration<float>>(stopwatch.elapsed()).count() << ",";
                out << iterations << ",";
                for(float delta : options.deltas) {
                    out << calculateConfidence(delta, iterations, averageM) << ",";
                }
                out << std::endl;
            }
        }
    }

    float elapsedTime = std::chrono::duration_cast<std::chrono::duration<float>>(stopwatch.stop()).count();
    std::vector<float> confidences;

    if(minimum <= options.epsilon) {
        for(float delta : options.deltas) {
            confidences.push_back(1.0);
        }
    } else {
        long double averageM = 0.0;
        if(sampleBins.size() == 0) {
            averageM = 1;
        } else {
            for( const auto& [key, val] : sampleBins ) {
                averageM += pow(1.0 - (key/config.binningFactor), iterations) * val;
            }
            averageM = averageM / iterations;
        }
        //cout << "AVERAGE M: " << averageM << endl << endl;

        for(float delta : options.deltas) {
            confidences.push_back(calculateConfidence(delta, iterations, averageM));
        }
    }
        
    stopwatch.reset();

    if(options.periodicProgress > 0 ) {
        auto& out = (fout.is_open()) ? fout : std::cout;

        out << elapsedTime << ",";
        out << iterations << ",";
        for(float conf : confidences) {
            out << conf << ",";
        }
        out << std::endl;
    }

    if(fout.is_open()) {
        fout.close();
    }

    return Result {
        .consistent = (minimum <= options.epsilon),
        .confidences = confidences,
        .minimum = minimum,
        .iterations = iterations,
        .elapsedTime = elapsedTime
    };
}

long double ModelGuardImpl::calculateVolume(const Eigen::Ref<const Eigen::ArrayX2f> &space) {
    return (space.col(0) -  space.col(1)).prod();
}

void ModelGuardImpl::random(Eigen::Ref<RowMajorArray> result) {
    for(int i=0; i<result.rows(); i++) {
        for(int j=0; j<result.cols(); j++) {
            result(i,j) = randomizers.at(j)();
        }
    }
}

long double ModelGuardImpl::calculateStateVolume(double distance, const Eigen::Ref<const RowMajorArray> state) {
    long double volume = 1.0;
    
    auto stateSpace = model->getStateSpace();
    for(int i=0; i<model->getStateDim(); i++) {
        double lower = std::max((double)(state(0,i) - distance), (double)stateSpace(i,0));
        double upper = std::min((double)(state(0,i) + distance), (double)stateSpace(i,1));

        volume *= (upper - lower);
    }

    return volume;
}

long double ModelGuardImpl::calculateConfidence(const float delta, const long int K, const long double avgM) {
    long double c = sqrt((log(2)-log(delta))/(2*K)) + 1e-20;
    long double ex = 0.0;

    if(K/c < 1000) {
        ex = 2*exp(-2*K*pow(c,2));
    }
    return std::max(1-((1-ex)/(delta-ex)) * (avgM+c), 0.0L);
}

ModelGuardImpl::~ModelGuardImpl() {
    
}

} // endnamespace modelguard::impl