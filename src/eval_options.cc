/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "modelguard/eval_options.h"

#include <numeric>
#include <chrono>
#include <string>

namespace modelguard {

EvalOptions EvalOptions::Default() {
    return EvalOptions {
        .epsilon = 0,
        .deltas = {0.25},
        .iterationLimit = std::numeric_limits<long int>::max(),
        .timeLimit = std::chrono::nanoseconds(std::numeric_limits<int64_t>::max()),
        .periodicProgress = -1,
        .progressFile = std::string()
    };
}

EvalOptions& EvalOptions::withEpsilon(float epsilon) {
    this->epsilon = epsilon;
    return *this;
}

EvalOptions& EvalOptions::withDeltas(const std::vector<float> deltas) {
    this->deltas = deltas;
    return *this;
}

EvalOptions& EvalOptions::withIterationLimit(long int iterationLimit) {
    this->iterationLimit = iterationLimit;
    return *this;
}

EvalOptions& EvalOptions::withTimeLimit(std::chrono::nanoseconds timeLimit) {
    this->timeLimit;
    return *this;
}

EvalOptions& EvalOptions::withPeriodicProgress(int frequency) {
    this->periodicProgress = frequency;
    return *this;
}

EvalOptions& EvalOptions::withProgressFile(std::string filename) {
    this->progressFile = filename;
    return *this;
}

} // endnamespace modelguard