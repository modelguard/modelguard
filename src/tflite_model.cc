/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "modelguard/tflite_model.h"

#include <memory>

#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/c/common.h>

namespace modelguard {

TFLiteModel::TFLiteModel(std::shared_ptr<tflite::FlatBufferModel> tfmodel,
                    const Eigen::Ref<const Eigen::ArrayX2f> stateSpace,
                    const Eigen::Ref<const Eigen::ArrayX2f> inputSpace,
                    const Eigen::Ref<const Eigen::ArrayX2f> outputSpace,
                    const double lipschitz,
                    const int batchSize) 
                : ModelBase(stateSpace, inputSpace, outputSpace, lipschitz, batchSize)
                , tfmodel(tfmodel) {

}

TFLiteModel::TFLiteModel(const TFLiteModel &other)
    : TFLiteModel(other.tfmodel, other.stateSpace, other.inputSpace, other.outputSpace, other.lipschitz, other.batchSize) {

}

void TFLiteModel::initialize() {
    if(!initialized) {
        tflite::ops::builtin::BuiltinOpResolver resolver;
        tflite::InterpreterBuilder(*(tfmodel.get()), resolver)(&interpreter, 1);
        interpreter->SetNumThreads(1);

        int inputSize = getStateDim() + getInputDim();
        std::vector<int> inputShape = {batchSize, inputSize};

        int outputSize = getOutputDim();

        interpreter->ResizeInputTensor(0, inputShape);
        interpreter->AllocateTensors();

        inputBuffer = interpreter->typed_input_tensor<float>(0);
        outputBuffer = interpreter->typed_output_tensor<float>(0);

        inputAdaptor = std::make_unique<Eigen::Map<RowMajorArray>>(inputBuffer, batchSize, inputSize);
        inputAdaptor->fill(0);

        outputAdaptor = std::make_unique<Eigen::Map<RowMajorArray>>(outputBuffer, batchSize, outputSize);
        outputAdaptor->fill(0);
    }

    initialized = true;
}

Eigen::Ref<RowMajorArray> TFLiteModel::invoke(const Eigen::Ref<const RowMajorArray> &state, const Eigen::Ref<const RowVector> &inputs) {
    inputAdaptor->topLeftCorner(state.rows(), state.cols()) = state;

    if( (inputAdaptor->topRightCorner(1, inputs.cols()) != inputs).any() ) {
        inputAdaptor->rightCols(inputs.cols()) = inputs.replicate(batchSize, 1);
    }

    interpreter->Invoke();

    return outputAdaptor->topRows(state.rows());
}

TFLiteModel* TFLiteModel::clone() const {
    return new TFLiteModel(*this);
}

TFLiteModel::~TFLiteModel() {

}
} // endnamespace modelguard