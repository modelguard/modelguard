/*
ModelGuard - A tool for runtime validation of Lipschitz-continuous models

Copyright (c) 2021, Taylor J Carpenter, Radoslav Ivanov, Insup Lee, James Weimer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "stopwatch.h"

#include <chrono>

namespace modelguard::impl {

using namespace std::chrono;

void Stopwatch::start() {
    localStart = clock_t::now();
    running = true;
}

nanoseconds Stopwatch::stop(bool ignore) {
    if(ignore || !running) {
        return nanoseconds();
    }

    nanoseconds timeElapsed = elapsed();
    running = false;

    occurrences++;
    occurrencesLap++;

    accTime += timeElapsed;
    accTimeLap += timeElapsed;

    if (timeElapsed > maxElapsed) {
        maxElapsed = timeElapsed;
    }

    if (timeElapsed > maxElapsedLap) {
        maxElapsedLap = timeElapsed;
    }

    return timeElapsed;
}

void Stopwatch::reset() {
    running = false;
    occurrences = 0;
    occurrencesLap = 0;
    accTime = nanoseconds();
    accTimeLap = nanoseconds();

}

nanoseconds Stopwatch::lapReset() {
    nanoseconds lapTime = accTimeLap;

    accTimeLap = nanoseconds();
    occurrencesLap = 0;
    maxElapsedLap = nanoseconds();

    return lapTime;
}

nanoseconds Stopwatch::elapsed(bool lap) const {
    if(running) {
        return clock_t::now() - localStart;
    } else {
        return lap ? accTimeLap : accTime;
    }
}

int Stopwatch::totalOccurrences(bool lap) const {
    return lap ? occurrencesLap : occurrences;
}

nanoseconds Stopwatch::totalTime(bool lap) const {
    return lap ? accTimeLap : accTime;
}

nanoseconds Stopwatch::meanTime(bool lap) const {
    if(totalOccurrences(lap) == 0) {
        return nanoseconds(0);
    } else {
        return lap ? accTimeLap / occurrencesLap : accTime / occurrences;
    }
}

nanoseconds Stopwatch::maxTime(bool lap) const {
    return lap ? maxElapsedLap : maxElapsed;
}

bool Stopwatch::isRunning() const {
    return running;
}
}